﻿using SimpleVcs.Models;
using SimpleVcs.Models.Serialization;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace SimpleVcs
{
  class Program
  {
    static void Main(string[] args)
    {
      while (true)
      {
        Console.WriteLine("Choose Operation:");
        Console.WriteLine(" * commit");
        Console.WriteLine(" * checkout");
        Console.WriteLine(" * branch (create new branch)");
        Console.WriteLine(" * exit");
        
        var operation = Console.ReadLine();

        switch (operation.ToLower())
        {
          case "commit":
            Commit();
            break;
          case "checkout":
            Checkout();
            break;
          case "branch":
            CreateBranch();
            break;
          case "exit":
            return;
          default:
            Console.WriteLine("Invalid Option");
            break;
        }
      }
    }

    private static void CreateBranch()
    {
      Console.Write("Enter Source Branch Name: ");
      var sourceBranch = Console.ReadLine();

      Console.Write("Enter New Branch Name: ");
      var newBranch = Console.ReadLine();

      var source = Vcs.GetRef(sourceBranch);

      if (source == null)
      {
        Console.WriteLine($"Ref '{sourceBranch}' not found");
        return;
      }

      Vcs.StoreRef(newBranch, source);
    }

    private static void Commit()
    {
      Console.WriteLine("Enter Branch: ");
      var branch = Console.ReadLine();

      var parent = Vcs.GetRef(branch);

      var commit = new Commit
      {
        Parents = parent == null ? new string[0] : new[] { parent },
        When = DateTimeOffset.UtcNow
      };

      commit.Tree = WorkingDirectory.ReadTree(new DirectoryInfo("."));

      Console.Write("Enter Author: ");
      commit.Author = Console.ReadLine();

      Console.Write("Enter Message: ");
      commit.Message = Console.ReadLine();

      var hash = Vcs.StoreObject(commit);

      Vcs.StoreRef(branch, hash);

      Console.WriteLine($"Commit Hash: {hash}");
    }

    static void Checkout()
    {
      Console.Write("Enter Branch: ");
      var branch = Console.ReadLine();

      var commitHash = Vcs.GetRef(branch);

      var commit = (Commit)Vcs.GetObject(commitHash);

      var directory = new DirectoryInfo(".");

      WorkingDirectory.WriteTree(commit.Tree, directory);
    }
  }
}
