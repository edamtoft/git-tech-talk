﻿using SimpleVcs.Models;
using SimpleVcs.Models.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace SimpleVcs
{
  public static class Vcs
  {
    public static string GetRef(string name)
    {
      var file = new FileInfo($"./_vcs/refs/{name}.txt");

      if (!file.Directory.Exists)
      {
        return null;
      }

      return File.ReadAllText(file.FullName);
    }

    public static void StoreRef(string name, string hash)
    {
      var file = new FileInfo($"./_vcs/refs/{name}.txt");

      if (!file.Directory.Exists)
      {
        file.Directory.Create();
      }

      File.WriteAllText(file.FullName, hash);
    }

    public static VcsObject GetObject(string hash)
    {
      var file = new FileInfo($"./_vcs/objects/{hash}.json");

      if (!file.Exists)
      {
        return null;
      }

      var json = File.ReadAllText(file.FullName);

      return JsonSerializer.Deserialize<VcsObject>(json, new JsonSerializerOptions
      {
        Converters =
        {
          new VcsConverter()
        }
      });
    }

    public static string StoreObject(VcsObject obj)
    {
      var json = JsonSerializer.Serialize(obj, new JsonSerializerOptions
      {
        WriteIndented = true,
        Converters =
        {
          new VcsConverter()
        }
      });

      using var sha1 = SHA1.Create();

      var hashBytes = sha1.ComputeHash(Encoding.UTF8.GetBytes(json));

      var hash = string.Concat(hashBytes.Take(6).Select(b => b.ToString("x2")));

      var file = new FileInfo($"./_vcs/objects/{hash}.json");

      if (!file.Directory.Exists)
      {
        file.Directory.Create();
      }

      if (!file.Exists)
      {
        File.WriteAllText(file.FullName, json);
      }

      return hash;
    }
  }
}
