﻿namespace SimpleVcs.Models
{
  public sealed class TreeEntry
  {
    public bool IsTree { get; set; }
    public string Hash { get; set; }
  }
}