﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SimpleVcs.Models.Serialization
{
  public sealed class VcsConverter : JsonConverter<VcsObject>
  {
    public override VcsObject Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
      if (reader.TokenType != JsonTokenType.StartObject)
      {
        throw new JsonException("Expected Object");
      }

      if (!reader.Read() || reader.TokenType != JsonTokenType.PropertyName)
      {
        throw new JsonException("Expected Property Name");
      }

      var typeName = reader.GetString();

      if (!reader.Read() || reader.TokenType != JsonTokenType.StartObject)
      {
        throw new JsonException("Expected Object");
      }
      
      VcsObject result = typeName switch
      {
        nameof(Blob) => JsonSerializer.Deserialize<Blob>(ref reader, options),
        nameof(Commit) => JsonSerializer.Deserialize<Commit>(ref reader, options),
        nameof(Tree) => JsonSerializer.Deserialize<Tree>(ref reader, options),
        _ => throw new NotImplementedException(),
      };
      
      if (!reader.Read() || reader.TokenType != JsonTokenType.EndObject)
      {
        throw new JsonException("Expected End Object");
      }

      return result;
    }

    public override void Write(Utf8JsonWriter writer, VcsObject value, JsonSerializerOptions options)
    {
      writer.WriteStartObject();

      var type = value.GetType();

      writer.WritePropertyName(type.Name);

      JsonSerializer.Serialize(writer, value, type, options);

      writer.WriteEndObject();
    }
  }
}
