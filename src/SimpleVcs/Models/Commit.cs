﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleVcs.Models
{
  public sealed class Commit : VcsObject
  {
    public string Author { get; set; }
    public string Message { get; set; }
    public DateTimeOffset When { get; set; }
    public string[] Parents { get; set; }
    public string Tree { get; set; }
  }
}
