﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleVcs.Models
{
  public class Tree : VcsObject
  {
    public Dictionary<string, TreeEntry> Entries { get; set; } = new Dictionary<string, TreeEntry>();
  }
}
