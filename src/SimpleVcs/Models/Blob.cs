﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleVcs.Models
{
  public sealed class Blob : VcsObject
  {
    public string Content { get; set; }
  }
}
