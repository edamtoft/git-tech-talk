﻿using SimpleVcs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SimpleVcs
{
  public static class WorkingDirectory
  {
    public static string ReadTree(DirectoryInfo directory)
    {
      var tree = new Tree();

      if (directory.Exists)
      {
        foreach (var subdirectory in directory.EnumerateDirectories().Where(dir => dir.Name != "_vcs"))
        {
          tree.Entries.Add(subdirectory.Name, new TreeEntry { Hash = ReadTree(subdirectory), IsTree = true });
        }

        foreach (var file in directory.EnumerateFiles())
        {
          var blob = new Blob
          {
            Content = File.ReadAllText(file.FullName)
          };

          tree.Entries.Add(file.Name, new TreeEntry { Hash = Vcs.StoreObject(blob), IsTree = false });
        }
      }

      return Vcs.StoreObject(tree);
    }

    public static void WriteTree(string hash, DirectoryInfo directory)
    {
      if (!directory.Exists)
      {
        directory.Create();
      }

      var tree = (Tree)Vcs.GetObject(hash);

      foreach (var (name, entry) in tree.Entries)
      {
        if (entry.IsTree)
        {
          WriteTree(entry.Hash, directory.CreateSubdirectory(name));
        }
        else
        {
          var blob = (Blob)Vcs.GetObject(entry.Hash);
          File.WriteAllText(Path.Combine(directory.FullName, name), blob.Content);
        }
      }

      foreach (var item in directory.EnumerateFileSystemInfos().Where(x => x.Name != "_vcs"))
      {
        if (!tree.Entries.ContainsKey(item.Name))
        {
          item.Delete();
        }
      }
    }
  }
}
